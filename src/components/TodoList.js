import React, { Component } from 'react';

export class TodoList extends Component {
  constructor(props){
    super(props);
  }

  render() {
    return (
      <div className="App">
        <h1>todoist</h1>
        <form onSubmit={this.props.onSubmit}>
          <input name="item" placeholder="Add a new item.">
          </input>
          <button type="submit">Add</button>
        </form>

        <ul>
        {this.props.items.map(item =>

              <li key={item.key}>{item.text} <button onClick={this.props.onClick.bind(this, item)}>X</button></li>
          )}
        </ul>

      </div>
    );
  }
}

TodoList.defaultProps = {
  items: [],
  editing: false
};
