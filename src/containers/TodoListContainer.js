import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { TodoList } from '../components/TodoList';

export class TodoListContainer extends Component {
  constructor(props){
    super(props);

    this.state = {
      items: [],
      editing: false
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.handleEdit = this.handleEdit.bind(this);
    this.handleSave = this.handleSave.bind(this);

  }

  /* Handles adding new items. */
  handleSubmit(e){
    e.preventDefault(); // Must be at top.
    if (e.target.value !== "") {
      let newItem = {
        text: e.target.item.value,
        key: Date.now()
      };

      this.setState(prevState => {
        return {
          items: prevState.items.concat(newItem)

        };
      });
    }
    console.log(this.state.items);
  }

  /* Handles removing items. */
  handleClick(item){
    this.setState(prevState => {
      return {
        items: prevState.items.filter(itm => itm != item)
      };
    });
  }

  handleEdit(item){
      console.log(this.state.editing);
      const newEdit = this.state.editing === false ? true : false;
      this.setState({
        editing: newEdit
      });
      console.log(newEdit);
      console.log(this.state.editing);
  }

  handleSave(e) {
    e.preventDefault();
    if (e.target.value !== "") {
      console.log(e.target.edit);
      let newItem = {
        text: e.target.edit.value,
        key: e.target.edit.key
      };
      //console.log(item.text);
      this.setState(prevState => {
        items: prevState.items[prevState.items.indexOf(newItem.key)] = newItem
      });

      //console.log(this.state.items.indexOf(item));
      console.log(this.state.items);
  }
  }

  render() {
    return (
      <TodoList items={this.state.items} onSubmit={this.handleSubmit} onClick={this.handleClick} handleEdit={this.handleEdit} editing={this.state.editing} handleSave={this.handleSave}/>
    );
  }
}
