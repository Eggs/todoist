import React, { Component } from 'react';
import './App.css';
import { TodoListContainer } from './containers/TodoListContainer';

class App extends Component {
  render() {
    return (

        <TodoListContainer />

    );
  }
}

export default App;
